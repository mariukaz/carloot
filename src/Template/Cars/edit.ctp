<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Car $car
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $car->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $car->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cars'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cars form large-9 medium-8 columns content">
    <?= $this->Form->create($car, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Edit Car') ?></legend>
		<?= $this->Html->image($car->photo_url, ['alt' => $car->photo]) ?>
        <?php
			echo $this->Form->input('photo', ['type' => 'file']);
		    echo $this->Form->control('mark_string');
            echo $this->Form->control('model_string');
            echo $this->Form->control('vin');
            echo $this->Form->control('import_cost');
            echo $this->Form->control('transportation_cost');
            echo $this->Form->control('upkeep');
            echo $this->Form->control('car_loot_cost');	
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
