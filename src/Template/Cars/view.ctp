<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Car $car
 */
?>
<nav class="large-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Car'), ['action' => 'edit', $car->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Car'), ['action' => 'delete', $car->id], ['confirm' => __('Are you sure you want to delete # {0}?', $car->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cars'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Car'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cars view large-10 medium-10 columns content">
    <h3><?= h($car->vin) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Photo') ?></th>
            <td><?= $this->Html->image($car->photo_url, ['alt' => $car->photo]) ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Mark string') ?></th>
            <td><?= h($car->mark_string) ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Model string') ?></th>
            <td><?= h($car->model_string) ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Vin') ?></th>
            <td><?= h($car->vin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Import Cost') ?></th>
            <td><?= h($car->import_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transportation Cost') ?></th>
            <td><?= h($car->transportation_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Car Loot Cost') ?></th>
            <td><?= h($car->car_loot_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($car->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Upkeep') ?></th>
            <td><?= $this->Number->format($car->upkeep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($car->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($car->updated) ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Selling cost') ?></th>
            <td><?= h($car->selling_cost) ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Last managed by') ?></th>
            <td><?= h($car->user->username) ?></td>
        </tr>
    </table>
</div>
