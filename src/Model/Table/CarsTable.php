<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
* Cars Model
*
* @property \App\Model\Table\ModelsTable|\Cake\ORM\Association\BelongsTo $Models
* @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
*
* @method \App\Model\Entity\Car get($primaryKey, $options = [])
* @method \App\Model\Entity\Car newEntity($data = null, array $options = [])
* @method \App\Model\Entity\Car[] newEntities(array $data, array $options = [])
* @method \App\Model\Entity\Car|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Car patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
* @method \App\Model\Entity\Car[] patchEntities($entities, array $data, array $options = [])
* @method \App\Model\Entity\Car findOrCreate($search, callable $callback = null, $options = [])
*
* @mixin \Cake\ORM\Behavior\TimestampBehavior
*/
class CarsTable extends Table
{

	/**
	* Initialize method
	*
	* @param array $config The configuration for the Table.
	* @return void
	*/
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('cars');
		$this->setDisplayField('vin');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Models', [
		'foreignKey' => 'model_id',
		'joinType' => 'INNER'
		]);
		$this->belongsTo('Users', [
		'foreignKey' => 'user_id',
		'joinType' => 'INNER'
		]);
		
		$this->addBehavior('Josegonzalez/Upload.Upload', [
            'photo' => [
				'fields' => [
                    'dir' => 'photo_dir'
                ],
				'keepFilesOnDelete' => false,
				'path' => 'webroot{DS}files{DS}{model}{DS}{field}{DS}{field-value:vin}{DS}'
				],
        ]);
	}

	/**
	* Default validation rules.
	*
	* @param \Cake\Validation\Validator $validator Validator instance.
	* @return \Cake\Validation\Validator
	*/
	public function validationDefault(Validator $validator)
	{
		$validator
		->integer('id')
		->allowEmpty('id', 'create')
		->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

		$validator
		->scalar('vin')
		->maxLength('vin', 17)
		->requirePresence('vin', 'create')
		->notEmpty('vin');

		$validator
		->scalar('import_cost')
		->requirePresence('import_cost', 'create')
		->notEmpty('import_cost');

		$validator
		->scalar('transportation_cost')
		->requirePresence('transportation_cost', 'create')
		->notEmpty('transportation_cost');

		$validator
		->decimal('upkeep')
		->requirePresence('upkeep', 'create')
		->notEmpty('upkeep');

		$validator
		->scalar('car_loot_cost')
		->requirePresence('car_loot_cost', 'create')
		->notEmpty('car_loot_cost');

		return $validator;
	}

	/**
	* Returns a rules checker object that will be used for validating
	* application integrity.
	*
	* @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	* @return \Cake\ORM\RulesChecker
	*/
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->isUnique(['id']));
		$rules->add($rules->existsIn(['model_id'], 'Models'));
		$rules->add($rules->existsIn(['user_id'], 'Users'));

		return $rules;
	}
	
	public function beforeSave($event, $entity, $options)
	{
		if($entity->mark_string && $entity->model_string) {
			$entity->model = $this->_buildModelMark($entity->mark_string, $entity->model_string);
		}
	}
	
	private function _buildModelMark($markString, $modelString) {
		$mark = $this->_buildMark($markString);
		return $this->_buildModel($modelString, $mark);
	}
	
	private function _buildMark($markString) {
		$marks = TableRegistry::get('Marks');
		
		$query = $marks->find()
		->where(['Marks.name' => $markString]);
		
		if($mark = $query->first()) {
			return $mark;
		}
		$mark = $marks->newEntity(['name' => $markString]);
		$marks->save($mark);
		
		return $mark;
		
	}
	
	private function _buildModel($modelString, $mark) {
		
		$query = $this->Models->find()
		->contain(['Marks'])
		->where([
		'Models.name' => $modelString,
		'Marks.id' => $mark->id
		]);
		
		if($model = $query->first()) {
			return $model;
		}

		$model = $this->Models->newEntity([
		'name' => $modelString,
		'mark_id' => $mark->id
		]);
		
		$this->Models->save($model);
		return $model;
		
	}
	
	
}
