<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\I18n\Number;
/**
 * Car Entity
 *
 * @property int $id
 * @property string $vin
 * @property string $import_cost
 * @property string $transportation_cost
 * @property float $upkeep
 * @property string $car_loot_cost
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 * @property int $model_id
 * @property int $user_id
 *
 * @property \App\Model\Entity\Model $model
 * @property \App\Model\Entity\User $user
 */
class Car extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vin' => true,
        'import_cost' => true,
        'transportation_cost' => true,
        'upkeep' => true,
        'car_loot_cost' => true,
        'created' => true,
        'updated' => true,
        'model_id' => true,
        'user_id' => true,
        'model' => true,
        'user' => true,
        '*' => true,
		
    ];
	
	/**
	 *  @brief Get model string
	 *  
	 *  @return string
	 */
	protected function _getModelString()
	{
		if (isset($this->_properties['model_string'])) {
			return $this->_properties['model_string'];
		}
		if (empty($this->model)) {
			return '';
		}
		return $this->model->name;
	}
	
	/**
	 *  @brief Get mark string
	 *  
	 *  @return string
	 */
	protected function _getMarkString()
	{
		if (isset($this->_properties['mark_string'])) {
			return $this->_properties['mark_string'];
		}
		if (empty($this->model)) {
			return '';
		}
		if (empty($this->model->mark)) {
			return '';
		}
		
		return $this->model->mark->name;
	}
	
	/**
	 *  @brief Generate photo url for html image
	 *  
	 *  @return string
	 */
	protected function _getPhotoUrl()
	{
		if (isset($this->_properties['photo_url'])) {
			return $this->_properties['photo_url'];
		}
		
		if (empty($this->photo)) {
			return '';
		}
		
		$photo_dir_parts = explode("/", $this->photo_dir);
		unset($photo_dir_parts[0]);
		$photo_dir = implode("/", $photo_dir_parts);
		
		return '../' . $photo_dir . '/' . $this->photo;
	}
	
	/**
	 *  @brief Calculate selling cost
	 *  
	 *  @return float
	 */
	protected function _getSellingCost()
	{
		if (isset($this->_properties['selling_cost'])) {
			return $this->_properties['selling_cost'];
		}
		
		$days = $this->_daysAgo($this->created);

		$cost = $this->toInt($this->import_cost) 
			+ ($this->toInt($this->import_cost) * $this->upkeep/100)
			+ $this->toInt($this->transportation_cost)
			+ $this->toInt($this->car_loot_cost)*$days;
		$max_cost = 2*($this->toInt($this->import_cost) 
			+ $this->toInt($this->transportation_cost));
		if($cost > $max_cost) {
			$cost = $max_cost;
		}
		return  Number::currency($cost);
	}
	
	/**
	 *  @brief Calculate how many days ago
	 *  
	 *  @param [in] $time Time to check
	 *  @return int
	 */
	private function _daysAgo($time)
	{
		$daylen = 60*60*24;
		$date1 = strtotime($time->i18nFormat('yyyy-MM-dd'));
		$date2 = strtotime(Time::now()->i18nFormat('yyyy-MM-dd'));
			
		return ($date2-$date1)/$daylen;
	}
	
	/**
	 *  @brief Postgress money to php float
	 *  
	 *  @param [in] $str money string
	 *  @return float
	 */
	private function toInt($str)
	{
		return preg_replace("/([^0-9\\.])/i", "", $str);
	}
}
