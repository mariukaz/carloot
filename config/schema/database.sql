--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cars; Type: TABLE; Schema: public; Owner: cake
--

CREATE TABLE cars (
    id integer NOT NULL,
    vin character varying(17) NOT NULL,
    import_cost money NOT NULL,
    transportation_cost money NOT NULL,
    upkeep numeric(4,2) NOT NULL,
    car_loot_cost money NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone,
    model_id integer NOT NULL,
    user_id integer NOT NULL,
    photo character varying(255),
    photo_dir character varying(255)
);


ALTER TABLE cars OWNER TO cake;

--
-- Name: cars_id_seq; Type: SEQUENCE; Schema: public; Owner: cake
--

CREATE SEQUENCE cars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cars_id_seq OWNER TO cake;

--
-- Name: cars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cake
--

ALTER SEQUENCE cars_id_seq OWNED BY cars.id;


--
-- Name: marks; Type: TABLE; Schema: public; Owner: cake
--

CREATE TABLE marks (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone
);


ALTER TABLE marks OWNER TO cake;

--
-- Name: marks_id_seq; Type: SEQUENCE; Schema: public; Owner: cake
--

CREATE SEQUENCE marks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE marks_id_seq OWNER TO cake;

--
-- Name: marks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cake
--

ALTER SEQUENCE marks_id_seq OWNED BY marks.id;


--
-- Name: models; Type: TABLE; Schema: public; Owner: cake
--

CREATE TABLE models (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    mark_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone
);


ALTER TABLE models OWNER TO cake;

--
-- Name: models_id_seq; Type: SEQUENCE; Schema: public; Owner: cake
--

CREATE SEQUENCE models_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE models_id_seq OWNER TO cake;

--
-- Name: models_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cake
--

ALTER SEQUENCE models_id_seq OWNED BY models.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: cake
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(64) NOT NULL,
    password character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone
);


ALTER TABLE users OWNER TO cake;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: cake
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO cake;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cake
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: cars id; Type: DEFAULT; Schema: public; Owner: cake
--

ALTER TABLE ONLY cars ALTER COLUMN id SET DEFAULT nextval('cars_id_seq'::regclass);


--
-- Name: marks id; Type: DEFAULT; Schema: public; Owner: cake
--

ALTER TABLE ONLY marks ALTER COLUMN id SET DEFAULT nextval('marks_id_seq'::regclass);


--
-- Name: models id; Type: DEFAULT; Schema: public; Owner: cake
--

ALTER TABLE ONLY models ALTER COLUMN id SET DEFAULT nextval('models_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: cake
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: cars; Type: TABLE DATA; Schema: public; Owner: cake
--

COPY cars (id, vin, import_cost, transportation_cost, upkeep, car_loot_cost, created, updated, model_id, user_id, photo, photo_dir) FROM stdin;
7	33333333333333333	$1,000.01	$100.10	10.00	$10.00	2017-09-04 14:52:23	\N	6	2	Untitled.png	webroot/files/Cars/photo/33333333333333333/
\.


--
-- Name: cars_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cake
--

SELECT pg_catalog.setval('cars_id_seq', 7, true);


--
-- Data for Name: marks; Type: TABLE DATA; Schema: public; Owner: cake
--

COPY marks (id, name, created, updated) FROM stdin;
6	BMW	2018-01-01 01:27:20	\N
7	Audis	2018-01-01 01:40:09	\N
\.


--
-- Name: marks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cake
--

SELECT pg_catalog.setval('marks_id_seq', 7, true);


--
-- Data for Name: models; Type: TABLE DATA; Schema: public; Owner: cake
--

COPY models (id, name, mark_id, created, updated) FROM stdin;
6	M3	6	2018-01-01 01:27:20	\N
7	B4	7	2018-01-01 01:40:09	\N
8	B4	6	2018-01-01 01:47:23	\N
\.


--
-- Name: models_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cake
--

SELECT pg_catalog.setval('models_id_seq', 8, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: cake
--

COPY users (id, username, password, created, updated) FROM stdin;
2	markom	$2y$10$TD3U7pr7ccLtjVo.WWBnX.huFSlMTlS.H7sTAAOaHehOiEDkxvW6m	2018-01-01 01:21:40	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cake
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Name: cars cars_pkey; Type: CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);


--
-- Name: marks marks_pkey; Type: CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY marks
    ADD CONSTRAINT marks_pkey PRIMARY KEY (id);


--
-- Name: models models_pkey; Type: CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: marks_name_uindex; Type: INDEX; Schema: public; Owner: cake
--

CREATE UNIQUE INDEX marks_name_uindex ON marks USING btree (name);


--
-- Name: models_id_uindex; Type: INDEX; Schema: public; Owner: cake
--

CREATE UNIQUE INDEX models_id_uindex ON models USING btree (id);


--
-- Name: users_username_uindex; Type: INDEX; Schema: public; Owner: cake
--

CREATE UNIQUE INDEX users_username_uindex ON users USING btree (username);


--
-- Name: cars cars_models_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY cars
    ADD CONSTRAINT cars_models_id_fk FOREIGN KEY (model_id) REFERENCES models(id);


--
-- Name: cars cars_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY cars
    ADD CONSTRAINT cars_users_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: models models_marks_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cake
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_marks_id_fk FOREIGN KEY (mark_id) REFERENCES marks(id);


--
-- PostgreSQL database dump complete
--

