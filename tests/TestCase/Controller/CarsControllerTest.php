<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CarsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CarsController Test Case
 */
class CarsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.marks',
        'app.models',
        'app.users',
        'app.cars'
    ];

	
	
	/**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/cars/index');

		$this->assertResponseOk();
		$this->assertResponseContains('Vin');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testIndexFail()
    {
		$this->get('/cars/index');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/cars/index']);
	}
	

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/cars/view/1');

		$this->assertResponseOk();
		$this->assertResponseContains('Vin');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testViewFail()
    {
		$this->get('/cars/view/1');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/cars/view/1']);
	}
	
    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/cars/add');

		$this->assertResponseOk();
		$this->assertResponseContains('Vin');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testAddFail()
    {
		$this->get('/cars/add');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/cars/add']);
	}
	
	/**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/cars/edit/1');

		$this->assertResponseOk();
		$this->assertResponseContains('Vin');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testEditFail()
    {
		$this->get('/cars/edit/1');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/cars/edit/1']);
	}
}
