<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.marks',
        'app.models',
        'app.cars',
        'app.users'
    ];

    /**
     * Test login method
     *
     * @return void
     */
    public function testLogin()
    {
        $this->get('/users/login');

        $this->assertResponseOk();
		$this->assertResponseContains('Username');
		$this->assertResponseContains('Password');
    }
	
	/**
     * Test login method
     *
     * @return void
     */
    public function testLoginForm()
    {
        $this->get('/users/login');

        $this->assertResponseOk();
		$this->assertResponseContains('Username');
		$this->assertResponseContains('Password');
    }
	
	/**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/users/index');

		$this->assertResponseOk();
		$this->assertResponseContains('Username');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testIndexFail()
    {
		$this->get('/users/index');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/users/index']);
	}
	

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/users/view/1');

		$this->assertResponseOk();
		$this->assertResponseContains('Username');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testViewFail()
    {
		$this->get('/users/view/1');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/users/view/1']);
	}
	
    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/users/add');

		$this->assertResponseOk();
		$this->assertResponseContains('Username');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testAddFail()
    {
		$this->get('/users/add');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/users/add']);
	}
	
	/**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
		// Set session data
		$this->session([
			'Auth' => [
				'User' => [
					'id' => 1,
					'username' => 'demo',
					// other keys.
				]
			]
		]);
		$this->get('/users/edit/1');

		$this->assertResponseOk();
		$this->assertResponseContains('Username');
    }
	
	/**
     * Test view method
     *
     * @return void
     */
    public function testEditFail()
    {
		$this->get('/users/edit/1');
		$this->assertRedirect(['controller' => 'Users', 'action' => 'login', 'redirect'=>'/users/edit/1']);
	}
	
	
}
