<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Mark;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Mark Test Case
 */
class MarkTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Mark
     */
    public $Mark;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Mark = new Mark();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mark);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
