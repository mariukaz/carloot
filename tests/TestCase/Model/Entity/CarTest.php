<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Car;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * App\Model\Entity\Car Test Case
 */
class CarTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Car
     */
    public $Car;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
		$this->loadFixtures('Marks');
		$this->loadFixtures('Models');
		$this->loadFixtures('Users');
		$this->loadFixtures('Cars');
        $this->Car = new Car();
		$this->Cars = TableRegistry::get('Cars');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Car);
        unset($this->Cars);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
	
	/**
     * Test _getModelString
     *
     * @return void
     */
    public function test_getModelString()
    {
		$expected = 'Lorem ipsum dolor sit amet';
		$id = 1;
		
        $car = $this->Cars->get($id, [
            'contain' => ['Models'=>['Marks'], 'Users']
        ]);
       
        $this->assertEquals($expected, $car->model_string);
    }
	
	/**
     * Test _getMarkString
     *
     * @return void
     */
    public function test_getMarkString()
    {
		
		$expected = 'Bmw';
		$id = 1;
		
        $car = $this->Cars->get($id, [
            'contain' => ['Models'=>['Marks'], 'Users']
        ]);
       
        $this->assertEquals($expected, $car->mark_string);
    }
	
	
	/**
     * Test _getPhotoUrl
     *
     * @return void
     */
    public function test_getPhotoUrl()
    {
		
		$expected = '../files/Cars/photo/2222/test.jpg';
		$id = 1;
		
        $car = $this->Cars->get($id, [
            'contain' => ['Models'=>['Marks'], 'Users']
        ]);
       
        $this->assertEquals($expected, $car->photo_url);
    }
	
	/**
     * Test _getSellingCost
     *
     * @return void
     */
    public function test_getSellingCost()
    {
				
		$expected = '$3,045.40';
		$id = 1;
		
		$now = new Time('2018-04-12 12:22:30');
		Time::setTestNow($now);
		
        $car = $this->Cars->get($id, [
            'contain' => ['Models'=>['Marks'], 'Users']
        ]);
       
        $this->assertEquals($expected, $car->selling_cost);
    }
	
	/**
     * Test _getSellingCostLongTime
     *
     * @return void
     */
    public function test_getSellingCostLongTime()
    {
				
		$expected = '$4,000.40';
		$id = 1;
		
		$now = new Time('2040-04-12 12:22:30');
		Time::setTestNow($now);	
		
        $car = $this->Cars->get($id, [
            'contain' => ['Models'=>['Marks'], 'Users']
        ]);
       
        $this->assertEquals($expected, $car->selling_cost);
    }
	
}
