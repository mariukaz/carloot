<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Model;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Model Test Case
 */
class ModelTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Model
     */
    public $Model;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Model = new Model();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Model);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
