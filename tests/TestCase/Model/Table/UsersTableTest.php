<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    public $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.marks',
        'app.models',
        'app.cars',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => UsersTable::class];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
     /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
		//all good test
        $data = [
			'username' => 'demo_testas',
			'password' => 'demopass',
		];
		$user = $this->Users->newEntity($data);
		$this->assertEmpty($user->errors());
		
		//empty username
        $data = [
			'username' => '',
			'password' => 'demopass',
		];
		$user = $this->Users->newEntity($data);
		$this->assertNotEmpty($user->errors());
		
		//username not set
		$data = [
			'password' => 'demopass'
		];
		$user = $this->Users->newEntity($data);
		$this->assertNotEmpty($user->errors());
		
		//password not set
		$data = [
			'username' => 'demo'
		];
		$user = $this->Users->newEntity($data);
		$this->assertNotEmpty($user->errors());
    }
}
