<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
* App\Model\Table\CarsTable Test Case
*/
class CarsTableTest extends TestCase
{

	/**
	* Test subject
	*
	* @var \App\Model\Table\CarsTable
	*/
	public $Cars;

	/**
	* Fixtures
	*
	* @var array
	*/
	public $fixtures = [
	'app.marks',
	'app.models',
	'app.cars',
	'app.users'
	];

	/**
	* setUp method
	*
	* @return void
	*/
	public function setUp()
	{
		parent::setUp();
		$config = TableRegistry::exists('Cars') ? [] : ['className' => CarsTable::class];
		$this->Cars = TableRegistry::get('Cars', $config);
	}

	/**
	* tearDown method
	*
	* @return void
	*/
	public function tearDown()
	{
		unset($this->Cars);

		parent::tearDown();
	}

	/**
	* Test initialize method
	*
	* @return void
	*/
	public function testInitialize()
	{
		$this->markTestIncomplete('Not implemented yet.');
	}

	/**
	* Test validationDefault method
	*
	* @return void
	*/
	public function testValidationDefault()
	{
		//all good test
		$data = [
		'vin' => '12345678901234567',
		'import_cost' => 10.10,
		'transportation_cost' => 10.10,
		'upkeep' => 10,10,
		'car_loot_cost' => 100,
		'model_id' => 1,
		'user_id' => 1,
		];
		$car = $this->Cars->newEntity($data);
		$this->assertEmpty($car->errors());
		
		//vin empty
		$data = [
		'import_cost' => 10.10,
		'transportation_cost' => 10.10,
		'upkeep' => 10,10,
		'car_loot_cost' => 100,
		'model_id' => 1,
		'user_id' => 1,
		];
		$car = $this->Cars->newEntity($data);
		$this->assertNotEmpty($car->errors());
	}

	/**
	* Test buildRules method
	*
	* @return void
	*/
	public function testBuildRules()
	{
		$this->markTestIncomplete('Not implemented yet.');
	}
	
	/**
	* Test beforeSave method
	*
	* @return void
	*/
	public function testBeforeSave() {
		$this->markTestIncomplete('Not implemented yet.');
	}
	
	/**
	* Test _buildModelMark method
	*
	* @return void
	*/
	public function test_buildModelMark() {
		$this->markTestIncomplete('Not implemented yet.');
	}
	
	
	/**
	* Test _buildMark method
	*
	* @return void
	*/
	public function test_buildMark() {
		$this->markTestIncomplete('Not implemented yet.');
	}
	
	/**
	* Test _buildModel method
	*
	* @return void
	*/
	public function test_buildModel() {
		$this->markTestIncomplete('Not implemented yet.');
	}
}
