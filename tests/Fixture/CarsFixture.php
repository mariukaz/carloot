<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CarsFixture
 *
 */
class CarsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'vin' => ['type' => 'string', 'length' => 17, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'import_cost' => ['type' => 'string', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'transportation_cost' => ['type' => 'string', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'upkeep' => ['type' => 'decimal', 'length' => 4, 'default' => null, 'null' => false, 'comment' => null, 'precision' => 2, 'unsigned' => null],
        'car_loot_cost' => ['type' => 'string', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => '2017-12-31 01:01:39.261104', 'null' => false, 'comment' => null, 'precision' => null],
        'updated' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'model_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'photo' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
		'photo_dir' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
		'_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'cars_models_id_fk' => ['type' => 'foreign', 'columns' => ['model_id'], 'references' => ['models', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'cars_users_id_fk' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'vin' => 'Lorem ipsum dol',
            'import_cost' => 1000.1,
            'transportation_cost' => 1000.1,
            'upkeep' => 1.5,
            'car_loot_cost' => 10.1,
            'created' => 1514684049,
            'updated' => 1514684049,
            'model_id' => 1,
            'user_id' => 1,
            'photo' => 'test.jpg',
            'photo_dir' => 'webroot/files/Cars/photo/2222',
        ],
		[
            'id' => 2,
            'vin' => 'Lorem ipsum dol',
            'import_cost' => 1000.1,
            'transportation_cost' => 1000.1,
            'upkeep' => 1.5,
            'car_loot_cost' => 10.1,
            'created' => 1514684049,
            'updated' => 1514684049,
            'model_id' => 2,
            'user_id' => 2,
			'photo' => 'test2.jpg',
            'photo_dir' => 'webroot/files/Cars/photo/444',
        ],
    ];
}
